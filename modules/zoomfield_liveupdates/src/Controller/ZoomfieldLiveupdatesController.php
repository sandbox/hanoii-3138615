<?php

namespace Drupal\zoomfield_liveupdates\Controller;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\HtmlCommand;
use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a route controller for entity autocomplete form elements.
 */
class ZoomfieldLiveupdatesController extends ControllerBase {

  public function refresh(Request $request, $meeting_id) {

    $element = [
      '#type' => 'zoomfield_liveupdates',
      '#meeting_id' => $meeting_id,
    ];

    $response = new AjaxResponse();
    $response->addCommand(new HtmlCommand(NULL, $element));
    return $response;
  }

}
