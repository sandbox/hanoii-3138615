<?php

namespace Drupal\zoomfield_liveupdates\Plugin\Field\FieldFormatter;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'Live updates' formatter.
 *
 * @FieldFormatter(
 *   id = "zoomfield_liveupdates",
 *   label = @Translation("Live Updates"),
 *   field_types = {
 *     "zoomfield"
 *   }
 * )
 */
class ZoomfieldLiveUpdatesFormatter extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'interval' => '30',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $extra['interval'] = [
      '#title' => $this->t('Refresh interval'),
      '#description' => $this->t('The interval, in seconds, in which a request is made to the server to fetch any meeting updates. If 0, refresh will be disabled.'),
      '#type' => 'number',
      '#default_value' => $this->getSetting('interval'),
      '#min' => 0,
    ];

    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];
    $entity = $items->getEntity();
    $settings = $this->getSettings();

    foreach ($items as $delta => $item) {
      $element[$delta] = [
        '#type' => 'zoomfield_liveupdates',
        '#meeting_id' => $item->value,
        "#refresh" => [
          'interval' => $settings['interval'],
        ],
      ];
    }
    return $element;
  }

}
