<?php

namespace Drupal\zoomfield_liveupdates\Element;

use Drupal\Component\Datetime\DateTimePlus;
use Drupal\Component\Utility\Html as HtmlUtility;
use Drupal\Core\Render\Element\RenderElement;
use Drupal\Core\Url;
use GuzzleHttp\Exception\RequestException;

/**
 * Provides a render element for an Zoom widget that pulls information from the
 * Zoom API and auto-updates.
 *
 * @RenderElement("zoomfield_liveupdates")
 */
class ZoomfieldLiveupdates extends RenderElement {

  /**
   * {@inheritdoc}
   */
  public function getInfo() {
    $class = get_class($this);
    return [
      '#theme' => 'zoomfield_liveupdates_widget',
      '#meeting' => [],
      '#refresh' => [],
      '#pre_render' => [
        [$class, 'preRenderWidget'],
      ],
      '#attributes' => [
        'class' => ['zoomfield-liveupdates-widget'],
      ],
    ];
  }

  /**
   * Pre-render callback: Renders a Zoom Widget #markup.
   *
   * @return array
   *   The passed-in element containing a rendered link in '#markup'.
   */
  public static function preRenderWidget($element) {
    $meeting_id = $element['#meeting_id'];

    $zoom_client = \Drupal::service('zoomapi.client');
    $messenger = \Drupal::service('messenger');

    if (!$zoom_client->validateConfiguration()) {
      $messenger->addError(
        t('Please <a href=":url">configure ZoomAPI</a>.', [
          ':url' => Url::fromRoute('zoomapi.settings')->toString(),
        ])
      );
    }
    try {
      $response = $zoom_client->request('get', '/meetings/' . $meeting_id);
      $meeting['id'] = $response['id'];
      $meeting['status'] = $response['status'];
      $meeting['join_url'] = $response['join_url'];
      $meeting['start_time'] = DateTimePlus::createFromDateTime(new \DateTime($response['start_time']));
      $meeting['start_time']->setTimezone(new \DateTimeZone(date_default_timezone_get()));
      $meeting['zoomapi_response'] = $response;
      $element['#meeting'] = $meeting;
    }
    catch (RequestException $e) {
      $meeting['status'] = 'invalid';
    }

    if (!empty($element["#refresh"]['interval'])) {
      $element['#theme_wrappers'] = [
        'container' => [
          '#attributes' => [
            'class' => 'zoomfield-liveupdates-wrapper zoomfield-liveupdates-use-ajax',
            'data-zoomfield-liveupdates-id' => $meeting_id,
            'data-zoomfield-liveupdates-interval' => $element["#refresh"]['interval'],
            'data-zoomfield-liveupdates-initial-status' => $meeting['status'],
            'id' => HtmlUtility::getUniqueId('zoomfield-liveupdates-widget'),
          ],
        ]
      ];
      $element['#attached'] = [
        'library' => [
          'zoomfield_liveupdates/liveupdates',
        ]
      ];
    }

    $element['#attributes']['data-zoomfield-liveupdates-status'] = $meeting['status'];

    return $element;
  }
}
