(function ($, window, Drupal, drupalSettings) {
  Drupal.behaviors.zoomfield_liveupdates = {
    attach: function attach(context, settings) {
      $('.zoomfield-liveupdates-use-ajax').once('zoomfield-liveupdates-ajax').each(function () {
        var element = this;

        // @see Drupal.behaviors.AJAX.attach().
        var elementSettings = {};
        elementSettings.url = Drupal.url('zoomfield_liveupdates/refresh/' + $(this).attr('data-zoomfield-liveupdates-id'));
        elementSettings.event = 'load';
        elementSettings.progress = { type: 'throbber' };
        elementSettings.base = $(element).attr('id');
        elementSettings.element = element;
        elementSettings.wrapper = $(element).attr('id');
        var ajax = Drupal.ajax(elementSettings);

        var status = $(element).attr('data-zoomfield-liveupdates-initial-status');
        var interval = $(element).attr('data-zoomfield-liveupdates-interval');

        if (status && status != 'invalid' && interval > 0) {
          setInterval(
            function() {
              $(element).trigger('load');
            },
            interval * 1000
          );
          $(element).trigger('load');
        }

      });
    }
  }
})(jQuery, window, Drupal, drupalSettings);
