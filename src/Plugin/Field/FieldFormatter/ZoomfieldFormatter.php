<?php

namespace Drupal\zoomfield\Plugin\Field\FieldFormatter;

use Drupal\Component\Utility\Html;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'link' formatter.
 *
 * @FieldFormatter(
 *   id = "zoomfield_link",
 *   label = @Translation("Meeting Link"),
 *   field_types = {
 *     "zoomfield"
 *   }
 * )
 */
class ZoomfieldFormatter extends FormatterBase implements ContainerFactoryPluginInterface {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'title' => 'Please join #@meeting',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $extra['title'] = [
      '#title' => $this->t('Link title'),
      '#description' => $this->t('The link title, plese use <em>@meeting</em> as a placeholder.'),
      '#type' => 'textfield',
      '#default_value' => $this->getSetting('title'),
    ];

    return $extra;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $element = [];

    foreach ($items as $delta => $item) {
      $meeting_id = $item->value;
      $element[$delta] = [
        '#type' => 'link',
        '#title' => $this->t(Html::escape($this->getSetting('title')), ['@meeting' => $meeting_id]),
        '#url' => Url::fromUri('https://zoom.us/j/' . $meeting_id),
        '#options' => [
          'attributes' => [
            'target' => '_blank',
          ],
        ],
      ];
    }
    return $element;
  }

}
