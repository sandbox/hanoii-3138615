<?php

namespace Drupal\zoomfield\Plugin\Field\FieldType;

use Drupal\Component\Utility\Random;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Field\FieldItemBase;
use Drupal\Core\Field\FieldStorageDefinitionInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\TypedData\DataDefinition;
use Drupal\Core\TypedData\MapDataDefinition;
use Drupal\Core\Url;

/**
 * Plugin implementation of the 'zoomfield' field type.
 *
 * @FieldType(
 *   id = "zoomfield",
 *   label = @Translation("Zoom Meeting ID"),
 *   description = @Translation("Stores a Zoom Meeting ID."),
 *   default_widget = "zoomfield_default",
 *   default_formatter = "zoomfield_link",
 *   constraints = {"ZoomfieldId" = {}}
 * )
 */
class ZoomfieldItem extends FieldItemBase {

  /**
   * {@inheritdoc}
   */
  public static function propertyDefinitions(FieldStorageDefinitionInterface $field_definition) {
    $properties['value'] = DataDefinition::create('string')
      ->setLabel(t('Zoom Meeting ID'))
      ->setRequired(TRUE);

    return $properties;
  }

  /**
   * {@inheritdoc}
   */
  public static function schema(FieldStorageDefinitionInterface $field_definition) {
    return [
      'columns' => [
        // https://support.zoom.us/hc/en-us/articles/201362373-What-is-a-Meeting-ID-
        // Leaving room for more characters
        'value' => [
          'description' => 'The URI of the link.',
          'type' => 'varchar',
          'length' => 24,
        ],
      ],
      'indexes' => [
        'value' => [['value', 11]],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function isEmpty() {
    $value = $this->get('value')->getValue();
    return $value === NULL || $value === '';
  }

  /**
   * {@inheritdoc}
   */
  public static function generateSampleValue(FieldDefinitionInterface $field_definition) {
    $values['value'] = mt_rand(100, 999) . ' ' . mt_rand(100, 999) . ' ' . mt_rand(1000, 9999);
    return $values;
  }


  /**
   * {@inheritdoc}
   */
  public function setValue($values, $notify = TRUE) {
    $values['value'] = preg_replace('/[ -]+/', '', $values['value']);
    parent::setValue($values, $notify);
  }

}
