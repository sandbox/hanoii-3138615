<?php

namespace Drupal\zoomfield\Plugin\Field\FieldWidget;

use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\Plugin\Field\FieldWidget\StringTextfieldWidget;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'link' widget.
 *
 * @FieldWidget(
 *   id = "zoomfield_default",
 *   label = @Translation("Zoom Meeting ID"),
 *   field_types = {
 *     "zoomfield"
 *   }
 * )
 */
class ZoomfieldWidget extends StringTextfieldWidget {

}
