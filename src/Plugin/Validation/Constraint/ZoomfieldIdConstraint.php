<?php

namespace Drupal\zoomfield\Plugin\Validation\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * Defines an access validation constraint for links.
 *
 * @Constraint(
 *   id = "ZoomfieldId",
 *   label = @Translation("Zoom Meeting ID is of the proper format.", context = "Validation"),
 * )
 */
class ZoomfieldIdConstraint extends Constraint {

  public $message = "The Zoom Meeting ID '@value' is not valid.";

}
