<?php

namespace Drupal\zoomfield\Plugin\Validation\Constraint;

use Drupal\zoomfield\Plugin\Field\FieldType\ZoomfieldItem;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

/**
 * Constraint validator for links receiving data allowed by its settings.
 */
class ZoomfieldIdConstraintValidator extends ConstraintValidator {

  /**
   * {@inheritdoc}
   */
  public function validate($value, Constraint $constraint) {
    if (!($value instanceof ZoomfieldItem)) {
      throw new UnexpectedTypeException($value, ZoomfieldItem::class);
    }

    if ($value->isEmpty()) {
        return;
    }
    $meeting_id = $value->get('value')->getValue();

    $has_not_numbers = preg_match('/[^0-9]/', $meeting_id);
    $invalid_length = strlen($meeting_id) < 9 || strlen($meeting_id) > 11;

    if ($has_not_numbers || $invalid_length) {
      $this->context->addViolation($constraint->message, ['@value' => $meeting_id]);
    }
  }

}
